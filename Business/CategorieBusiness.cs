﻿using Fr.EQL.AI110.DLD_GE.DataAccess;
using Fr.EQL.AI110.DLD_GE.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Business
{
    public class CategorieBusiness
    {
        public List<Categorie> GetCategories()
        {
            CategorieDAO catDAO = new CategorieDAO();
            return catDAO.GetAll();
        }
        public Categorie GetCategorieById(int id)
        {
            CategorieDAO categorieDAO = new CategorieDAO();
            return categorieDAO.GetById(id);
        }
    }
}
