﻿using Fr.EQL.AI110.DLD_GE.DataAccess;
using Fr.EQL.AI110.DLD_GE.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Business
{
    public class SouscategorieBusiness
    {
        public List<Souscategorie> GetSousCategories()
        {
            SouscategorieDAO souCatDAO = new SouscategorieDAO();
            return souCatDAO.GetAll();
        }
        public List<Souscategorie> GetAllSousCategoriesById(int id)
        {
            SouscategorieDAO sousCategorieDAO = new SouscategorieDAO();
            return sousCategorieDAO.GetAllById(id);
        }
        public Souscategorie GetSousCategorieById(int id)
        {
            SouscategorieDAO souscategorieDAO = new SouscategorieDAO();
            return souscategorieDAO.GetById(id);
        }
    }
}
