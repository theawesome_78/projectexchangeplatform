﻿using Fr.EQL.AI110.DLD_GE.DataAccess;
using Fr.EQL.AI110.DLD_GE.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Business
{
    public class UniteBusiness
    {
        public List<Unite> GetUnites()
        {
            UniteDAO uniteDAO = new UniteDAO();
            return uniteDAO.GetAll();
        }
        public List<Unite> GetUnitesByIdProduit(int id)
        {
            UniteDAO uniteDAO = new UniteDAO();
            return uniteDAO.GetAllByIdProduit(id);
        }
    }
}
