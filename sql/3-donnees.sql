-- MySQL dump 10.13  Distrib 8.0.27, for Win64 (x86_64)
--
-- Host: localhost    Database: dld_ge_bdd
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `achatcoupon`
--

LOCK TABLES `achatcoupon` WRITE;
/*!40000 ALTER TABLE `achatcoupon` DISABLE KEYS */;
INSERT INTO `achatcoupon` VALUES (1,6,'2021-12-10 00:00:00');
/*!40000 ALTER TABLE `achatcoupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `adherent`
--

LOCK TABLES `adherent` WRITE;
/*!40000 ALTER TABLE `adherent` DISABLE KEYS */;
INSERT INTO `adherent` VALUES (4,NULL,1,17,NULL,'rue rollin','LEPERS','Julien','Mr','1987-01-10 00:00:00',601010101,'julienlepers@hotmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,NULL,6,3,NULL,'rue sebastopol','ARRIAL','Alexis','Mr','1987-01-10 00:00:00',618601876,'potrunks@hotmail.com',NULL,NULL,'Potrunks','Trunks92!',NULL,NULL,NULL),(6,NULL,6,5,'b','rue de l\'insurrection parisienne','LEE','Alexandre','Mr','1987-01-10 00:00:00',602020202,'lee.alexandre@wanadoo.fr',NULL,NULL,'AlexandreLee','AlexandreLee94!',NULL,NULL,NULL),(7,NULL,14,1,NULL,'rue tannebourg','MOUCHARD','Baptiste','Autre','1987-01-10 00:00:00',603030303,'baptiste@outlook.com',NULL,NULL,'Baptiste','Baptiste94058!',NULL,NULL,NULL),(8,NULL,10,36,NULL,'bd du collet','BLACKPINK','Lisa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `adherent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `categorie`
--

LOCK TABLES `categorie` WRITE;
/*!40000 ALTER TABLE `categorie` DISABLE KEYS */;
INSERT INTO `categorie` VALUES (1,'Produits laitiers (sauf graisses)'),(2,'Matières grasses et huiles'),(3,'Glaces et sorbets'),(4,'Fruits et lègumes'),(5,'Céréales et dérivés de graines, racines et tubercules de lègumes secs (sauf boulangerie)'),(6,'Produits de boulangerie'),(7,'Viande'),(8,'Poisson et produits de la pêche'),(9,'Œufs et produits à base d\'œufs');
/*!40000 ALTER TABLE `categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `compositionstockage`
--

LOCK TABLES `compositionstockage` WRITE;
/*!40000 ALTER TABLE `compositionstockage` DISABLE KEYS */;
/*!40000 ALTER TABLE `compositionstockage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `conditionconservation`
--

LOCK TABLES `conditionconservation` WRITE;
/*!40000 ALTER TABLE `conditionconservation` DISABLE KEYS */;
INSERT INTO `conditionconservation` VALUES (1,'+4'),(2,'-20'),(3,'temperature ambiante');
/*!40000 ALTER TABLE `conditionconservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `don`
--

LOCK TABLES `don` WRITE;
/*!40000 ALTER TABLE `don` DISABLE KEYS */;
INSERT INTO `don` VALUES (1,6,16,20,13,NULL,NULL,1,'2021-12-30 00:00:00','2021-12-13 00:00:00',NULL,NULL,NULL,'2021-12-13 00:00:00',1,'rue lepante',NULL),(10,6,15,22,11,NULL,NULL,1,'2021-12-30 00:00:00','2021-12-13 00:00:00',NULL,NULL,NULL,'2021-12-13 00:00:00',1,'rue test',NULL),(11,4,1,10,5,NULL,NULL,6,'2021-12-30 00:00:00','2021-12-13 00:00:00',NULL,NULL,NULL,'2021-12-13 00:00:00',1,'rue machin',NULL),(12,7,13,1,1,10,NULL,2,'2021-12-30 00:00:00','2021-12-13 00:00:00',NULL,NULL,NULL,'2021-12-20 12:39:53',52,'rue a coté','b'),(13,4,7,14,1,NULL,NULL,1,'2021-12-30 00:00:00','2021-12-14 00:00:00',NULL,NULL,NULL,'2021-12-13 00:00:00',2,'rue michelin',NULL),(14,7,1,6,14,NULL,NULL,1,'2021-12-30 00:00:00','2021-12-13 00:00:00',NULL,NULL,NULL,'2021-12-13 00:00:00',1,'rue tannebourg',NULL),(15,5,1,7,6,NULL,NULL,5,'2021-12-30 00:00:00','2021-12-15 01:16:40',NULL,NULL,NULL,'2021-12-13 00:00:00',3,'rue sebastopol',NULL),(16,6,4,4,6,NULL,NULL,50,'2021-12-30 00:00:00','2021-12-15 09:29:25',NULL,NULL,NULL,'2021-12-13 00:00:00',5,'rue de l\'insurrection parisienne',NULL),(17,5,5,12,6,NULL,NULL,1,'2021-12-30 00:00:00','2021-12-15 09:53:20',NULL,NULL,NULL,'2021-12-13 00:00:00',3,'rue sebastopol',NULL),(18,7,5,1,14,NULL,NULL,1,'2021-12-17 00:00:00','2021-12-15 09:54:35',NULL,NULL,NULL,'2021-12-13 00:00:00',1,'rue tannebourg',NULL),(19,4,5,1,1,NULL,NULL,1,'2021-12-30 00:00:00','2021-12-15 10:25:26',NULL,NULL,NULL,'2021-12-13 00:00:00',17,'rue rollin',NULL),(20,4,5,1,1,NULL,NULL,1,'2021-12-19 00:00:00','2021-12-15 10:44:27',NULL,NULL,NULL,'2021-12-13 00:00:00',17,'rue rollin',NULL),(21,6,5,1,6,NULL,NULL,500,'2021-12-19 00:00:00','2021-12-15 12:34:02',NULL,NULL,NULL,'2021-12-13 00:00:00',5,'rue de l\'insurrection parisienne',NULL),(22,4,4,20,1,NULL,NULL,10,'2021-12-21 00:00:00','2021-12-15 14:21:32',NULL,NULL,NULL,'2021-12-13 00:00:00',17,'rue rollin',NULL),(23,4,12,21,1,5,NULL,1,'2021-12-31 00:00:00','2021-12-20 12:39:53',NULL,NULL,NULL,'2021-12-20 12:39:53',12,'rue de jean-jaures','\0'),(24,7,13,14,1,12,NULL,3,'2022-01-06 00:00:00','2021-12-20 16:19:14',NULL,NULL,NULL,'2021-12-13 00:00:00',52,'rue a coté','\0'),(25,6,14,11,1,11,NULL,12,'2022-01-07 00:00:00','2021-12-21 10:38:15',NULL,NULL,NULL,NULL,52,'rue a coté','\0'),(26,5,2,4,1,9,NULL,500,'2022-01-09 00:00:00','2021-12-21 11:27:57',NULL,NULL,NULL,NULL,52,'rue a coté','\0'),(27,5,4,5,6,NULL,NULL,2,'2022-01-09 00:00:00','2021-12-21 11:38:44',NULL,NULL,NULL,'2021-12-21 11:38:44',3,'rue sebastopol','\0'),(28,7,7,13,14,NULL,NULL,2,'2022-01-09 00:00:00','2021-12-21 17:29:06',NULL,NULL,NULL,'2021-12-21 11:38:44',1,'rue tannebourg','\0'),(29,4,7,1,1,8,NULL,5,'2022-01-07 00:00:00','2021-12-22 10:51:52',NULL,NULL,NULL,NULL,12,'rue de jean-jaures','\0'),(30,4,1,21,1,NULL,NULL,10,'2021-12-31 00:00:00','2021-12-22 10:53:17',NULL,NULL,NULL,'2021-12-21 11:38:44',17,'rue rollin','\0'),(32,4,7,16,1,NULL,NULL,2,'2022-01-08 00:00:00','2021-12-22 12:25:33',NULL,NULL,'2021-12-22 13:02:07','2021-12-22 12:25:33',17,'rue rollin','\0');
/*!40000 ALTER TABLE `don` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `emplacementstockage`
--

LOCK TABLES `emplacementstockage` WRITE;
/*!40000 ALTER TABLE `emplacementstockage` DISABLE KEYS */;
INSERT INTO `emplacementstockage` VALUES (5,1,1,1),(6,1,1,2),(7,1,1,3),(8,1,1,4),(9,2,2,1),(10,2,2,2),(11,3,1,1),(12,3,2,2),(13,4,1,1),(14,4,3,2),(15,5,1,1),(16,5,1,2),(17,5,1,3),(18,6,2,1),(19,7,3,1),(20,7,3,2),(21,8,1,1),(22,9,2,1),(23,10,3,1),(24,10,3,2),(25,11,1,1),(26,11,1,2),(27,11,1,3),(28,12,2,1),(29,13,3,1),(30,14,1,1),(31,14,1,2),(32,14,1,3);
/*!40000 ALTER TABLE `emplacementstockage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `forfait`
--

LOCK TABLES `forfait` WRITE;
/*!40000 ALTER TABLE `forfait` DISABLE KEYS */;
INSERT INTO `forfait` VALUES (1,1,1),(2,5,4),(3,10,7),(4,20,13);
/*!40000 ALTER TABLE `forfait` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `infrastockage`
--

LOCK TABLES `infrastockage` WRITE;
/*!40000 ALTER TABLE `infrastockage` DISABLE KEYS */;
INSERT INTO `infrastockage` VALUES (1,1,1,NULL,12,'rue de jean-jaures',NULL),(2,2,1,'premier etage a droite ',52,'rue a coté','b'),(3,2,1,'a gauche apres entree',52,'rue a coté','b'),(4,4,5,NULL,2,'route d\'introuvable','b'),(5,3,2,NULL,162,'Grand Boulevard',NULL),(6,5,5,NULL,5,'grand boulevard mouchard',NULL),(7,5,5,NULL,5,'grand boulevard mouchard',NULL),(8,6,11,NULL,1,'rue de Versailles',NULL),(9,6,11,NULL,1,'rue de Versailles',NULL),(10,6,11,NULL,1,'rue de Versailles',NULL),(11,7,12,NULL,2,'rue des champs elysées',NULL),(12,8,9,NULL,756,'rue des marseillais',NULL),(13,8,10,NULL,756,'rue des marseillais',NULL),(14,9,6,NULL,5,'Rue de Moldavie','b');
/*!40000 ALTER TABLE `infrastockage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `joursemaine`
--

LOCK TABLES `joursemaine` WRITE;
/*!40000 ALTER TABLE `joursemaine` DISABLE KEYS */;
INSERT INTO `joursemaine` VALUES (1,'lun'),(2,'mar'),(3,'mer'),(4,'jeu'),(5,'ven'),(6,'sam'),(7,'dim');
/*!40000 ALTER TABLE `joursemaine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mesurer`
--

LOCK TABLES `mesurer` WRITE;
/*!40000 ALTER TABLE `mesurer` DISABLE KEYS */;
INSERT INTO `mesurer` VALUES (6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(18,1),(20,1),(21,1),(22,1),(4,2),(5,2),(6,2),(7,2),(8,2),(9,2),(10,2),(11,2),(15,2),(17,2),(4,3),(5,3),(6,3),(7,3),(8,3),(9,3),(10,3),(11,3),(15,3),(17,3),(20,3),(21,3),(22,3),(4,4),(5,4),(6,4),(7,4),(8,4),(9,4),(10,4),(11,4),(15,4),(17,4),(20,4),(21,4),(22,4),(1,5),(2,5),(3,5),(12,5),(13,5),(14,5),(16,5),(19,5),(1,6),(2,6),(3,6),(12,6),(13,6),(14,6),(16,6),(19,6),(1,7),(2,7),(3,7),(12,7),(13,7),(14,7),(16,7),(19,7),(21,8),(22,8),(21,9),(22,9),(4,10),(5,10),(6,10),(7,10),(8,10),(9,10),(17,10),(21,12),(22,12),(1,13),(12,13),(13,13),(14,13),(16,13),(2,14),(3,14),(10,14),(11,14),(15,14),(17,14),(19,14),(20,16),(22,16);
/*!40000 ALTER TABLE `mesurer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `motifdesincription`
--

LOCK TABLES `motifdesincription` WRITE;
/*!40000 ALTER TABLE `motifdesincription` DISABLE KEYS */;
/*!40000 ALTER TABLE `motifdesincription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `motifreclamation`
--

LOCK TABLES `motifreclamation` WRITE;
/*!40000 ALTER TABLE `motifreclamation` DISABLE KEYS */;
/*!40000 ALTER TABLE `motifreclamation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `partenaire`
--

LOCK TABLES `partenaire` WRITE;
/*!40000 ALTER TABLE `partenaire` DISABLE KEYS */;
INSERT INTO `partenaire` VALUES (1,1,NULL,'KFC',12,NULL,'rue de jean-jaures',201020305,'kfc@m.com',NULL,NULL,NULL,'kfc1','kfc1',NULL,NULL,NULL),(2,1,NULL,'Macdonalds',54,'b','rue de commandant Mouchard',234567897,'macdo@d.fr',NULL,NULL,NULL,'macdo1','kfc1',NULL,NULL,NULL),(3,2,NULL,'Louis Vuitton',162,NULL,'Grand boulevard',298765434,'lv@d.fr',NULL,NULL,NULL,'lv1','lv1',NULL,NULL,NULL),(4,5,NULL,'APF',2,'b','route d\'introuvable',202020205,'apf@top.fr',NULL,NULL,NULL,'apf1','apf1',NULL,NULL,NULL),(5,5,NULL,'Burger Factory',5,NULL,'grand boulevard Mouchard',123456786,'bf@f.fr',NULL,NULL,NULL,'bf1','bf1',NULL,NULL,NULL),(6,11,NULL,'ChateauDeVersailles',1,NULL,'Rue de versailles',128756781,'cvd@z.fr',NULL,NULL,NULL,'cvd1','cvd1',NULL,NULL,NULL),(7,12,NULL,'HC-HauteGamme',2,NULL,'rue des champs elysees',233445567,'hchg@d.com',NULL,NULL,NULL,'hchg1','hchg1',NULL,NULL,NULL),(8,10,NULL,'RelaisMarsaille',756,NULL,'rue des marsaillais',656968515,'sd@gg.com',NULL,NULL,NULL,'rm1','rm1',NULL,NULL,NULL),(9,6,NULL,'MoldavieBusiness',5,'b','Rue de moldavie',256967466,'zdsssd@gmail.com',NULL,NULL,NULL,'mb1','mb1',NULL,NULL,NULL);
/*!40000 ALTER TABLE `partenaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `periodeindisponibilite`
--

LOCK TABLES `periodeindisponibilite` WRITE;
/*!40000 ALTER TABLE `periodeindisponibilite` DISABLE KEYS */;
/*!40000 ALTER TABLE `periodeindisponibilite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `plagehoraire`
--

LOCK TABLES `plagehoraire` WRITE;
/*!40000 ALTER TABLE `plagehoraire` DISABLE KEYS */;
INSERT INTO `plagehoraire` VALUES ('0000-00-00 09:00:00','0000-00-00 12:00:00',1,1,1),('0000-00-00 14:00:00','0000-00-00 21:00:00',2,1,1),('0000-00-00 09:00:00','0000-00-00 12:00:00',3,2,1),('0000-00-00 14:00:00','0000-00-00 21:00:00',4,2,1),('0000-00-00 09:00:00','0000-00-00 12:00:00',5,3,1),('0000-00-00 14:00:00','0000-00-00 21:00:00',6,3,1),('0000-00-00 09:00:00','0000-00-00 12:00:00',7,4,1),('0000-00-00 14:00:00','0000-00-00 21:00:00',8,4,1),('0000-00-00 09:00:00','0000-00-00 12:00:00',9,5,1),('0000-00-00 14:00:00','0000-00-00 21:00:00',10,5,1),('0000-00-00 09:00:00','0000-00-00 12:00:00',11,1,2),('0000-00-00 14:00:00','0000-00-00 21:00:00',12,6,3);
/*!40000 ALTER TABLE `plagehoraire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `produit`
--

LOCK TABLES `produit` WRITE;
/*!40000 ALTER TABLE `produit` DISABLE KEYS */;
INSERT INTO `produit` VALUES (1,1,1,'Lait'),(2,1,2,'Crème fraiche légère'),(3,1,2,'Crème fraiche épaisse'),(4,1,3,'Feta'),(5,1,3,'Mozarella'),(6,1,4,'Camembert'),(7,1,4,'Saint Nectaire'),(8,1,5,'Comté'),(9,1,5,'Cantal'),(10,1,6,'Yaourt nature'),(11,1,6,'Yaourt aux fruits'),(12,3,7,'Huile d\'olive'),(13,3,7,'Huile de noix'),(14,3,7,'Huile de tournesol'),(15,1,8,'Saindoux'),(16,3,8,'Huile de foie de morrue'),(17,1,9,'Beurre'),(18,2,10,'Cornet de glace'),(19,2,11,'Sorbet vanille'),(20,3,12,'Banane'),(21,3,12,'Pomme'),(22,3,15,'Tomate');
/*!40000 ALTER TABLE `produit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `quantiteforfait`
--

LOCK TABLES `quantiteforfait` WRITE;
/*!40000 ALTER TABLE `quantiteforfait` DISABLE KEYS */;
INSERT INTO `quantiteforfait` VALUES (3,1,2),(4,1,1);
/*!40000 ALTER TABLE `quantiteforfait` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `reclamation`
--

LOCK TABLES `reclamation` WRITE;
/*!40000 ALTER TABLE `reclamation` DISABLE KEYS */;
/*!40000 ALTER TABLE `reclamation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `reservation`
--

LOCK TABLES `reservation` WRITE;
/*!40000 ALTER TABLE `reservation` DISABLE KEYS */;
/*!40000 ALTER TABLE `reservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `souscategorie`
--

LOCK TABLES `souscategorie` WRITE;
/*!40000 ALTER TABLE `souscategorie` DISABLE KEYS */;
INSERT INTO `souscategorie` VALUES (1,1,'Lait et produits liquides lactés'),(2,1,'Crème et produits similaires'),(3,1,'Fromage frais'),(4,1,'Fromage à  pate crue'),(5,1,'Fromage à  pate cuite'),(6,1,'Yaourts et desserts lactés'),(7,2,'Huiles végétales'),(8,2,'Huiles et matières grasses animales'),(9,2,'Beurre et matières grasses tartinables'),(10,3,'Glace de consommation'),(11,3,'Sorbets'),(12,4,'Fruits frais'),(13,4,'Fruits transformés'),(14,4,'Fruits secs'),(15,4,'Légumes frais (incluant champignons)'),(16,4,'Légumes séchés (incluant champignons)'),(17,4,'Légumes transformés (en conserve, tartinables, pulpes et sauces...)'),(18,5,'Graines céréalières entières, éclatées ou en flocons'),(19,5,'Céréales pour petit déjeuner (incluant flocons d\'avoine)'),(20,5,'Pâtes, nouilles et produits similaires'),(21,6,'Pain et produits associés'),(22,6,'Gâteaux, biscuits et tartes'),(23,7,'Viande crue'),(24,7,'Viande crue transformée (viande séchée, viande marinée...)'),(25,7,'Viande cuite'),(26,8,'Poissons et produits de la pèche frais'),(27,8,'Poissons et produits de la pèche frais transformés'),(28,8,'Poissons et produits de la pèche cuits'),(29,9,'Produits à base d\'œufs liquides (incluant desserts type crème anglaise)'),(30,9,'Produits à base d\'œufs séchés ou cuits');
/*!40000 ALTER TABLE `souscategorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tailleemplacement`
--

LOCK TABLES `tailleemplacement` WRITE;
/*!40000 ALTER TABLE `tailleemplacement` DISABLE KEYS */;
INSERT INTO `tailleemplacement` VALUES (1,'small','20*20*20'),(2,'moyen','30*30*30'),(3,'big','40*40*40');
/*!40000 ALTER TABLE `tailleemplacement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `typestockage`
--

LOCK TABLES `typestockage` WRITE;
/*!40000 ALTER TABLE `typestockage` DISABLE KEYS */;
INSERT INTO `typestockage` VALUES (1,1,'box'),(2,2,'refregire'),(3,3,'etagere'),(4,3,'box');
/*!40000 ALTER TABLE `typestockage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `unite`
--

LOCK TABLES `unite` WRITE;
/*!40000 ALTER TABLE `unite` DISABLE KEYS */;
INSERT INTO `unite` VALUES (1,'pièce'),(2,'milligrame'),(3,'grame'),(4,'kilograme'),(5,'millilitre'),(6,'centilitre'),(7,'litre'),(8,'demi-douzaine'),(9,'douzaine'),(10,'part'),(11,'botte'),(12,'cagette'),(13,'bouteille'),(14,'pot'),(15,'grappe'),(16,'régime'),(17,'cagette');
/*!40000 ALTER TABLE `unite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ville`
--

LOCK TABLES `ville` WRITE;
/*!40000 ALTER TABLE `ville` DISABLE KEYS */;
INSERT INTO `ville` VALUES (1,75005,'Paris-5éme'),(2,75004,'Paris-4éme'),(3,75003,'Paris-3éme'),(4,75002,'Paris-2éme'),(5,75001,'Paris-1er'),(6,94600,'Choisy-Le-Roi'),(7,94400,'Vitry-Sur-Seine'),(8,94800,'Villejuif'),(9,13004,'Marseille-4éme'),(10,13008,'Marseille-8éme'),(11,78000,'Versailles'),(12,92370,'Chaville'),(13,92310,'Sévres'),(14,94058,'Le Perreux sur Marne');
/*!40000 ALTER TABLE `ville` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-22 13:07:39
