﻿using Fr.EQL.AI110.DLD_GE.Business;
using Fr.EQL.AI110.DLD_GE.Entities;
using Microsoft.AspNetCore.Mvc;
using Fr.EQL.AI110.DLD_GE.WebApp2.Models;



namespace Fr.EQL.AI110.DLD_GE.WebApp2.Controllers
{
    public class DonController : Controller
    {
        public int controller;
        // 0 : Créer don à domicile
        // 1 : Créer don chez un partenaire ou box
        // 2 : Don recherché
        // 3 : Don reservé

        private CreateDonViewModel Model { get; set; }
        // Permet choix de l'adherent connecté
        [HttpGet]
        public IActionResult Index()
        {
            CreateDonViewModel model = new CreateDonViewModel();

            AdherentBusiness adhBU = new AdherentBusiness();
            model.Adherents = adhBU.GetAdherents();

            return View(model);
        }
        [HttpPost]
        public IActionResult Index(CreateDonViewModel model)
        {
            DonBusiness bu = new DonBusiness();
            model.DonDetails = bu.GetAllDonsFromIdAdh(model.SelectedAdherent.Idadh);

            AdherentBusiness adhBU = new AdherentBusiness();
            model.SelectedAdherent = adhBU.GetAdherentbyId(model.SelectedAdherent.Idadh);

            return View("DisplayDon", model);
        }

        [HttpGet]
        public IActionResult Edit()
        {
            CreateDonViewModel model = new CreateDonViewModel();

            AdherentBusiness adhBU = new AdherentBusiness();
            model.Adherents = adhBU.GetAdherents();

            return View(model);
        }

        [HttpPost]
        public IActionResult CatchAdherent(CreateDonViewModel model)
        {
            AdherentBusiness adhBU = new AdherentBusiness();
            model.SelectedAdherent = adhBU.GetAdherentbyId(model.SelectedAdherent.Idadh);

            CategorieBusiness catBU = new CategorieBusiness();
            model.Categories = catBU.GetCategories();

            return View("ChooseCategory", model);
        }

        [HttpPost]
        public IActionResult CatchCategory(CreateDonViewModel model)
        {
            AdherentBusiness adhBU = new AdherentBusiness();
            model.SelectedAdherent = adhBU.GetAdherentbyId(model.SelectedAdherent.Idadh);

            CategorieBusiness catBU = new CategorieBusiness();
            model.SelectedCategorie = catBU.GetCategorieById(model.SelectedCategorie.IdCategorie);

            SouscategorieBusiness subCatBU = new SouscategorieBusiness();
            model.SousCategories = subCatBU.GetAllSousCategoriesById(model.SelectedCategorie.IdCategorie);

            return View("ChooseSubCategory", model);
        }
        [HttpPost]
        public IActionResult CatchSubCategory(CreateDonViewModel model)
        {
            AdherentBusiness adhBU = new AdherentBusiness();
            model.SelectedAdherent = adhBU.GetAdherentbyId(model.SelectedAdherent.Idadh);

            CategorieBusiness catBU = new CategorieBusiness();
            model.SelectedCategorie = catBU.GetCategorieById(model.SelectedCategorie.IdCategorie);

            SouscategorieBusiness souscategorieBusiness = new SouscategorieBusiness();
            model.SelectedSousCategorie = souscategorieBusiness.GetSousCategorieById(model.SelectedSousCategorie.IdSousCategorie);

            ProduitBusiness produitBusiness = new ProduitBusiness();
            model.Produits = produitBusiness.GetProduitsById(model.SelectedSousCategorie.IdSousCategorie);

            return View("ChooseProduct", model);
        }
        public IActionResult CatchProduct(CreateDonViewModel model)
        {
           
            AdherentBusiness adhBU = new AdherentBusiness();
            model.SelectedAdherent = adhBU.GetAdherentbyId(model.SelectedAdherent.Idadh);

            CategorieBusiness catBU = new CategorieBusiness();
            model.SelectedCategorie = catBU.GetCategorieById(model.SelectedCategorie.IdCategorie);

            SouscategorieBusiness souscategorieBusiness = new SouscategorieBusiness();
            model.SelectedSousCategorie = souscategorieBusiness.GetSousCategorieById(model.SelectedSousCategorie.IdSousCategorie);

            ProduitBusiness produitBusiness = new ProduitBusiness();
            model.SelectedProduit = produitBusiness.GetProduitById(model.SelectedProduit.IdProduit);

            UniteBusiness uniteBusiness = new UniteBusiness();
            model.Unites = uniteBusiness.GetUnitesByIdProduit(model.SelectedProduit.IdProduit);

            return View("ChooseUnity", model);
    }
        [HttpPost]
        public IActionResult Edit (CreateDonViewModel model, int id)
        {

                AdherentBusiness adhBU = new AdherentBusiness();
                model.SelectedAdherent = adhBU.GetAdherentbyId(model.SelectedAdherent.Idadh);
                model.Idemplacementstockage = id;
                DonBusiness donBusiness = new DonBusiness();
                Don don = new Don();
                don.IdAdh = model.SelectedAdherent.Idadh;
                don.IdUnite = model.SelectedUnity.IdUnite;
                don.IdProduit = model.SelectedProduit.IdProduit;

                don.Quantite = model.Don.Quantite;
                don.DateLimite = model.Don.DateLimite;
                don.DateAjout = DateTime.Now;
                if (model.Idemplacementstockage != 0)
                    {
                        don.IdEmplacementStockage = model.Idemplacementstockage;
                    }
                if (don.IdEmplacementStockage == 0) 
                {
                    don.IdVille = model.SelectedAdherent.Idville;
                    don.NumRue = model.SelectedAdherent.Numrue;
                    don.LibeleVoie = model.SelectedAdherent.Libelevoie;
                    don.DateDepotStockage = DateTime.Now;

            }
            if (don.IdEmplacementStockage != 0) 
                {
                    don.IdVille = model.Idville;
                    don.NumRue = model.Numrue;
                    don.LibeleVoie = model.Libelevoie;
                    
                }
            donBusiness.SaveDon(don);
            return View("EditConfirm");
         
        }
        [HttpGet]
        public IActionResult ChoixAvantDepotAdresse(CreateDonViewModel model)
            {
                this.Model = model;
                return View(Model); 
        }

        [HttpPost]
        public IActionResult ChoixAvantDepotAdresse(CreateDonViewModel model, int id)
            {
            this.Model = model;
            VilleBusiness vBU = new VilleBusiness();
            model.Villes = vBU.GetVilles();
            InfrastockageBusiness infBU = new InfrastockageBusiness();
            model.InfrastockageDetails = infBU.ToutInfraDispo();
            return View(Model) ; 
        }

        [HttpGet]
        public IActionResult IndexInfraParVille(CreateDonViewModel model)
        {
            this.Model = model;
            VilleBusiness vBU = new VilleBusiness();
            Model.Villes = vBU.GetVilles();

            InfrastockageBusiness infBU = new InfrastockageBusiness();
            Model.InfrastockageDetails = infBU.ToutInfraDispo();
            return View(Model);
        }
        [HttpPost]
        public IActionResult IndexInfraParVille(CreateDonViewModel model, string no)
        {
            this.Model = model;
            VilleBusiness vBU = new VilleBusiness();
            Model.Villes = vBU.GetVilles();

            InfrastockageBusiness infBU = new InfrastockageBusiness();
            Model.InfrastockageDetails = infBU.ToutInfraDispoParVille(Model.Idville);

            return View(Model);
        }

        [HttpPost]
        public IActionResult InfoEmplacementPartenaire(CreateDonViewModel model,int id)
        {   
            //CreateDonViewModel Model = (CreateDonViewModel)TempData["ParametreProposeDon"];
            this.Model = model;
            EmplacementstockageBusiness empBU = new EmplacementstockageBusiness();
            Model.empsDetails = empBU.ListEmplacementDispoInfra(id);

            return View(Model);
        }
        [HttpPost]
        public IActionResult AdresseDepotEnrgistrement(CreateDonViewModel model,int id)
        {
            this.Model = model;
            Model.Idemplacementstockage = id;

        //}

            return View("EditConfirm",Model);

        }

        [HttpGet]
        public IActionResult InfosDon(int id, int controller)
        {

            DonDetailsViewModel details = new DonDetailsViewModel();

            // controller à fournir dans la méthode appelante
            details.ControllerOrigine = controller;
            details.Id = id;

            DonBusiness biz = new DonBusiness();
            details.DonDetails = biz.ShowDetails(id);

            return View(details);
        }
        public IActionResult DisplayDonFromAdh(int id)
        {
            DonBusiness bu = new DonBusiness();
            CreateDonViewModel model = new CreateDonViewModel();
            model.DonDetails = bu.GetAllDonsFromIdAdh(id);

            foreach (DonDetails d in model.DonDetails)
            {
                d.TimeUntilExpiration = d.DateLimite - DateTime.Now;
                d.DurationDays = d.TimeUntilExpiration.TotalDays;
            }

            AdherentBusiness adhBU = new AdherentBusiness();
            model.SelectedAdherent = adhBU.GetAdherentbyId(id);

            return View("DisplayDon", model);
        }
        public IActionResult ConfirmationAnnulation(int id)
        {
            DonBusiness biz = new DonBusiness();
            biz.CancelDon(id, DateTime.Now);
            return View();
        }
    }
}
