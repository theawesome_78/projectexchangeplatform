﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp2.Models;
using Fr.EQL.AI110.DLD_GE.DataAccess;
using Fr.EQL.AI110.DLD_GE.Entities;
using Fr.EQL.AI110.DLD_GE.Business;


namespace WebApp2.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Index()
        {
            DonBusiness DonBU = new DonBusiness();
            List<DonDetails> dons = DonBU.GetAllDon();
            return View(dons);
        }
    }
}