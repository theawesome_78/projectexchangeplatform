﻿using Microsoft.AspNetCore.Mvc;
using Fr.EQL.AI110.DLD_GE.Business;
using Fr.EQL.AI110.DLD_GE.Entities;
using Fr.EQL.AI110.DLD_GE.WebApp2.Models;

namespace Fr.EQL.AI110.DLD_GE.WebApp2.Controllers
{
    public class PartenaireController : Controller
    {
        private CreateDonViewModel Model { get; set; }
        [HttpGet]
        public IActionResult IndexPartenaireParVille()
        {
            PartenaireDetails model = new PartenaireDetails();

            VilleBusiness vBU = new VilleBusiness();
            model.Villes = vBU.GetVilles();

            PartenaireBusiness parBU = new PartenaireBusiness();
            model.PartenairesDetails = parBU.ToutPartenaireDetails();

            return View(model);
        }
        [HttpPost]
        public IActionResult IndexPartenaireParVille(PartenaireDetails model)
        {

            VilleBusiness vBU = new VilleBusiness();
            model.Villes = vBU.GetVilles();
            PartenaireBusiness partBU = new PartenaireBusiness();
            model.PartenairesDetails = partBU.ToutPartenaireParVille(model.Idville);
            return View(model);
        }
        
        public IActionResult InfoEmplacementPartenaire(int id)
        {
            
            EmplacementstockageBusiness empBU = new EmplacementstockageBusiness();
            List<EmplacementstockageDetails> model = empBU.ListEmplacementParPart(id);

            return View(model);

        }
       
        [HttpGet]
        public IActionResult IndexInfraParVille(CreateDonViewModel model)
        {
            this.Model = model;
            VilleBusiness vBU = new VilleBusiness();
            model.Villes = vBU.GetVilles();

            InfrastockageBusiness infBU = new InfrastockageBusiness();
            model.InfrastockageDetails = infBU.ToutInfraDispo();
            return View(model);
        }
        [HttpPost]
        public IActionResult IndexInfraParVille(CreateDonViewModel model, int id)
        {
            this.Model = model;
            VilleBusiness vBU = new VilleBusiness();
            model.Villes = vBU.GetVilles();

            InfrastockageBusiness infBU = new InfrastockageBusiness();
            model.InfrastockageDetails = infBU.ToutInfraDispoParVille(model.Idville);
            return View(model);
        }
    }
}
