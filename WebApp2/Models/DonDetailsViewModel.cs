﻿using Fr.EQL.AI110.DLD_GE.Entities;

namespace Fr.EQL.AI110.DLD_GE.WebApp2.Models
{
    public class DonDetailsViewModel
    {
        // Déclarer comment on est arrivé sur la page details (controller d'origine : "mes dons réservés", "mes dons proposés"...)
        public int ControllerOrigine { get; set; }
        public int Id { get; set; }
        public string NomProduit { get; set; }
        public string CatNom { get; set; }
        public string SousCatNom { get; set; }
        public string AdhPrenom { get; set; }
        public string VilleNom { get; set; }
        public DateTime DateLimite { get; set; }
        public int Quantite { get; set; }
        public string UniteNom { get; set; }
        public DonDetails DonDetails { get; set; }
        public override string? ToString()
        {
            return base.ToString();
        }
    }
}
