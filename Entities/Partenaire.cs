﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class Partenaire
    {
        public const int COMPLEMENT_MAX_LENGTH = 1;
        public const int Numtelephone_LENGTH = 10;
        #region ATTRIBUTES
        public int Idpartenaire { get; set; }
        public int Idville { get; set; }
        public int Idmotif { get; set; }
        public string Nomsociete { get; set; }
        public int Numrue { get; set; }

        //complement est 1 char
        [MaxLength(COMPLEMENT_MAX_LENGTH, ErrorMessage = "taille de champ incorrecte, 1 caractère max")]
        public char? Complement { get; set; }
        public string Libelevoie { get; set; }

        //Le partenaire peut renseigner plusieurs numéros
        //de téléphone mais chaque numéro doit être différent
        //les uns des autres.

        
        public int Numtelephone { get; set; }
        //Le mail saisi par le partenaire doit être unique par
        //rapport à tous les autres partenaire inscrit sur
        //l’application.Le mail doit contient « @ » et «. ».
        public string Mail { get; set; }
        public DateTime Datevalidationinscription { get; set; }
        public DateTime Dateinscription { get; set; }
        public DateTime Daterefus { get; set; }

        //Le login saisi par le partenaire doit être unique
        //par rapport aux autres partenaires inscrit.
        public string Login { get; set; }

        //Le mot de passe doit contenir au minimum 8 caractères et/ou numériques avec pour règle au moins 1 majuscule, 1 symbole et 1 numérique
        public string Mdp { get; set; }
        public DateTime Datedesinscription { get; set; }
        public DateTime Datedebutdesactivation { get; set; }
        public DateTime Datefindesactivation { get; set; }

        #endregion
        public Partenaire()
        {
        }

        public Partenaire(int idpartenaire, int idville, int idmotif, string nomsociete, int numrue, char? complement, string libelevoie, 
            int numtelephone, string mail, DateTime datevalidationinscription, DateTime dateinscription, DateTime daterefus, string login, 
            string mdp, DateTime datedesinscription, DateTime datedebutdesactivation, DateTime datefindesactivation)
        {
            Idpartenaire = idpartenaire;
            Idville = idville;
            Idmotif = idmotif;
            Nomsociete = nomsociete;
            Numrue = numrue;
            Complement = complement;
            Libelevoie = libelevoie;
            Numtelephone = numtelephone;
            Mail = mail;
            Datevalidationinscription = datevalidationinscription;
            Dateinscription = dateinscription;
            Daterefus = daterefus;
            Login = login;
            Mdp = mdp;
            Datedesinscription = datedesinscription;
            Datedebutdesactivation = datedebutdesactivation;
            Datefindesactivation = datefindesactivation;
        }
    }
}
