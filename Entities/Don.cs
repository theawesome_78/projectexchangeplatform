﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class Don
    {
        #region PROPERTIES
        public int IdDon { get; set; }
        public int IdAdh { get; set; } /*fk Adherent*/
        [Required(ErrorMessage = "Le numéro d'adhérent est obligatoire")]
        public int IdUnite { get; set; } /*fk Unite*/
        [Required(ErrorMessage = "L'unité de mesure est obligatoire")]
        public int IdProduit { get; set; } /*fk Produit*/
        [Required(ErrorMessage = "Le type de produit est obligatoire")]
        public int IdVille { get; set; } /*fk Unite*/
        [Required(ErrorMessage = "La ville est le code postal sont obligatoires")]
        public int IdEmplacementStockage { get; set; } /*fk Emplacementstockage*/
        public string Photo { get; set; }
        [Required(ErrorMessage = "Champ quantité obligatoire")]
        [Range(1.0, int.MaxValue, ErrorMessage = "Valeur supérieur à 0 obligatoire")]
        public int Quantite { get; set; }
        [Required(ErrorMessage = "La quantité est obligatoire")]
        public DateTime DateLimite { get; set; }
        [Required(ErrorMessage = "La date limite de récupération est obligatoire")]
        public DateTime DateAjout { get; set; }
        public DateTime DateRecuperationDonPerime { get; set; }
        public DateTime DateAnnulation { get; set; }
        public DateTime DateDepotStockage { get; set; }
        public int NumRue { get; set; }
        [Required(ErrorMessage = "Le numéro de voie est obligatoire")]
        public string LibeleVoie { get; set; }
        [Required(ErrorMessage = "Le libelé de voie est obligatoire")]
        public char Complement { get; set; }
        //attribue importée from produit pour page acceuil
        public string NomProduit { get; set; }
        //attribue importée from ville pour page acceuil
        public string Nom { get; set; }
        public string CatNom { get; set; }
        public string SousCatNom { get; set; }
        public string VilleNom { get; set; }
        #endregion

        #region CONSTRUCTORS
        public Don()
        {
        }

        public Don(int idDon, int idAdh, int idUnite, int idProduit, int idVille, int quantite, 
            DateTime dateLimite, DateTime dateAjout, int numRue, string libeleVoie)
        {
            IdDon = idDon;
            IdAdh = idAdh;
            IdUnite = idUnite;
            IdProduit = idProduit;
            IdVille = idVille;
            Quantite = quantite;
            DateLimite = dateLimite;
            DateAjout = dateAjout;
            NumRue = numRue;
            LibeleVoie = libeleVoie;
        }

        public Don(int idDon, int idAdh, int idUnite, int idProduit, int idVille, int idEmplacementStockage, 
            int quantite, DateTime dateLimite, DateTime dateAjout, int numRue, string libeleVoie, char complement)
        {
            IdDon = idDon;
            IdAdh = idAdh;
            IdUnite = idUnite;
            IdProduit = idProduit;
            IdVille = idVille;
            IdEmplacementStockage = idEmplacementStockage;
            Quantite = quantite;
            DateLimite = dateLimite;
            DateAjout = dateAjout;
            NumRue = numRue;
            LibeleVoie = libeleVoie;
            Complement = complement;
        }

        public Don(int idDon, int idAdh, int idUnite, int idProduit, 
            int idVille, int idEmplacementStockage, string photo, 
            int quantite, DateTime dateLimite, DateTime dateAjout, 
            DateTime dateRecuperationDonPerime, DateTime dateAnnulation, 
            DateTime dateDepotStockage, int numRue, string libeleVoie, 
            char complement, string nomProduit, string nom)
        {
            IdDon = idDon;
            IdAdh = idAdh;
            IdUnite = idUnite;
            IdProduit = idProduit;
            IdVille = idVille;
            IdEmplacementStockage = idEmplacementStockage;
            Photo = photo;
            Quantite = quantite;
            DateLimite = dateLimite;
            DateAjout = dateAjout;
            DateRecuperationDonPerime = dateRecuperationDonPerime;
            DateAnnulation = dateAnnulation;
            DateDepotStockage = dateDepotStockage;
            NumRue = numRue;
            LibeleVoie = libeleVoie;
            Complement = complement;
            NomProduit = nomProduit;
            Nom = nom;
        }

        public Don(int idDon, int idAdh, int idUnite, int idProduit, 
            int idVille, int idEmplacementStockage, string photo, 
            int quantite, DateTime dateLimite, DateTime dateAjout, 
            DateTime dateRecuperationDonPerime, DateTime dateAnnulation, 
            DateTime dateDepotStockage, int numRue, string libeleVoie, 
            char complement, string nomProduit, string nom, string catNom, 
            string sousCatNom, string villeNom) : this(idDon, idAdh, idUnite, 
                idProduit, idVille, idEmplacementStockage, photo, quantite, 
                dateLimite, dateAjout, dateRecuperationDonPerime, dateAnnulation, 
                dateDepotStockage, numRue, libeleVoie, complement, nomProduit, nom)
        {
            CatNom = catNom;
            SousCatNom = sousCatNom;
            VilleNom = villeNom;
        }

        #endregion
    }
}
