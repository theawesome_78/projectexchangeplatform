﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class DonDetails : Don
    {
        public string CatNom { get; set; }
        public string SousCatNom { get; set; }
        public string NomProduit { get; set; }
        public string VilleNom { get; set; }
        public string Status { get; set; }
        public string NomUnity { get; set; }
        public DateTime DateReservation { get; set; }
        public DateTime DateDelivery { get; set; }
        public TimeSpan TimeUntilExpiration { get; set; }
        public double DurationDays { get; set; }
        public string AdhPrenom { get; set; }
        public string AdhName { get; set; }
        public string UniteNom { get; set; }
        public int PostalCode { get; set; }

        public DonDetails()
        {
        }
        public DonDetails(string catNom, string sousCatNom, string nomProduit, string villeNom)
        {
            CatNom = catNom;
            SousCatNom = sousCatNom;
            NomProduit = nomProduit;
            VilleNom = villeNom;
        }

        public DonDetails(string catNom, string sousCatNom, string nomProduit, string villeNom, string adhPrenom, string uniteNom)
        {
            CatNom = catNom;
            SousCatNom = sousCatNom;
            NomProduit = nomProduit;
            VilleNom = villeNom;
            AdhPrenom = adhPrenom;
            UniteNom = uniteNom;
        }
    }
}
