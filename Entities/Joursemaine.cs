﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class Joursemaine
    {
        public int IdJourS { get; set; }
        public string LibelleJourS { get; set; }
    }
}
