﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.Entities
{
    public class Infrastockage
    {
        public const int COMPLEMENT_MAX_LENGTH = 1;
        #region ATTRIBUTES
        public int Idinfrastockage { get; set; }
        public int Idpartenaire { get; set; }
        public int Idville { get; set; }
        public string? Complementinfo { get; set; }
        public int Numrue { get; set; }
        public string Libelevoie { get; set; }

        //complement est 1 char
        [MaxLength(COMPLEMENT_MAX_LENGTH, ErrorMessage = "taille de champ incorrecte, 1 caractère max")]
        public char? Complement { get; set; }
        #endregion
    }
}
