﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GE.Entities;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GE.DataAccess
{
    public class EmplacementstockageDAO : DAO
    {
        public List<EmplacementstockageDetails> ListEmplacementDispoInfra(int Idinfrastockage)
        {
            List<EmplacementstockageDetails> listemp = new List<EmplacementstockageDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT i.numrue numrueInf, i.libelevoie libelevoieInf, 
                                i.complement complementInf, p.numrue numruePart, 
                                p.libelevoie libelevoiePart, p.complement complementPart, v.codepostal,
                                v.nom nomville,  e.numemplacement, t.dimension, e.idemplacementstockage,
                                d.idemplacementstockage iddonemplacement, v.idville
                                FROM partenaire p
                                JOIN infrastockage i ON i.idpartenaire = p.idpartenaire
                                JOIN emplacementstockage e ON e.idinfrastockage = i.idinfrastockage
                                JOIN tailleemplacement t ON t.idtaille = e.idtaille
                                LEFT OUTER JOIN don d ON d.idemplacementstockage = e.idemplacementstockage
                                JOIN ville v ON v.idville = i.idville
                                WHERE i.idinfrastockage = @idinfrastockage
                                AND d.idemplacementstockage is null
                                ";

            cmd.Parameters.Add(new MySqlParameter("idinfrastockage", Idinfrastockage));
            cnx.Open();

            DbDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                EmplacementstockageDetails empDispoPart = DataReaderToEntityEmpDispo(dr);
                listemp.Add(empDispoPart);
            }

            cnx.Close();
            return listemp;
        }

        private EmplacementstockageDetails DataReaderToEntityEmpDispo(DbDataReader dr)
        {
            EmplacementstockageDetails empDispoPart = new EmplacementstockageDetails();


            if (!dr.IsDBNull(dr.GetOrdinal("numrueInf")))
            {
                empDispoPart.NumrueInf = dr.GetInt32(dr.GetOrdinal("numrueInf"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("libelevoieInf")))
            {
                empDispoPart.LibelevoieInf = dr.GetString(dr.GetOrdinal("libelevoieInf"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("complementInf")))
            {
                empDispoPart.ComplementInf = dr.GetChar(dr.GetOrdinal("complementInf"));

            }
            empDispoPart.NumruePart = dr.GetInt32(dr.GetOrdinal("numruePart"));
            empDispoPart.LibelevoiePart = dr.GetString(dr.GetOrdinal("libelevoiePart"));
            if (!dr.IsDBNull(dr.GetOrdinal("complementPart")))
            {
                empDispoPart.ComplementPart = dr.GetChar(dr.GetOrdinal("complementPart"));
            }
            empDispoPart.Codepostal = dr.GetInt32(dr.GetOrdinal("codepostal"));
            empDispoPart.Nomville = dr.GetString(dr.GetOrdinal("nomville"));
            empDispoPart.Numemplacement = dr.GetInt32(dr.GetOrdinal("numemplacement"));
            empDispoPart.Dimension = dr.GetString(dr.GetOrdinal("dimension"));
            empDispoPart.Idemplacementstockage = dr.GetInt32(dr.GetOrdinal("idemplacementstockage"));
            if (!dr.IsDBNull(dr.GetOrdinal("iddonemplacement")))
            {
                empDispoPart.Iddonemplacement = dr.GetInt32(dr.GetOrdinal("iddonemplacement"));
            }
            empDispoPart.Idville = dr.GetInt32(dr.GetOrdinal("idville"));


            return empDispoPart;
        }
        public List<EmplacementstockageDetails> ListEmplacementParPart(int Idpartenaire)
        {
            List<EmplacementstockageDetails> listemp = new List<EmplacementstockageDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT i.numrue, i.complement, i.libelevoie , 
                                p.numrue numruePart, p.complement complementPart, p.libelevoie libelevoiePart, 
                                v.codepostal, v.nom, e.numemplacement,
                                t.dimension, v.idville, i.idinfrastockage
                                
                                FROM partenaire p
								JOIN infrastockage i ON i.idpartenaire = p.idpartenaire
								JOIN emplacementstockage e ON e.idinfrastockage = i.idinfrastockage
								JOIN tailleemplacement t ON t.idtaille = e.idtaille
                              
								LEFT OUTER JOIN don d ON d.idemplacementstockage = e.idemplacementstockage
								JOIN ville v ON v.idville = i.idville
                                WHERE p.idpartenaire = @idpartenaire
                                ";
            cmd.Parameters.Add(new MySqlParameter("idpartenaire", Idpartenaire));
            cnx.Open();

            DbDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                EmplacementstockageDetails empDispoPart = new EmplacementstockageDetails();


                if (!dr.IsDBNull(dr.GetOrdinal("numrue")))
                {
                    empDispoPart.NumrueInf = dr.GetInt32(dr.GetOrdinal("numrue"));
                }
                if (!dr.IsDBNull(dr.GetOrdinal("libelevoie")))
                {
                    empDispoPart.LibelevoieInf = dr.GetString(dr.GetOrdinal("libelevoie"));
                }
                if (!dr.IsDBNull(dr.GetOrdinal("complement")))
                {
                    empDispoPart.ComplementInf = dr.GetChar(dr.GetOrdinal("complement"));

                }
                empDispoPart.NumruePart = dr.GetInt32(dr.GetOrdinal("numruePart"));
                empDispoPart.LibelevoiePart = dr.GetString(dr.GetOrdinal("libelevoiePart"));
                if (!dr.IsDBNull(dr.GetOrdinal("complementPart")))
                {
                    empDispoPart.ComplementPart = dr.GetChar(dr.GetOrdinal("complementPart"));
                }
                empDispoPart.Codepostal = dr.GetInt32(dr.GetOrdinal("codepostal"));
                empDispoPart.Nomville = dr.GetString(dr.GetOrdinal("nom"));
                empDispoPart.Numemplacement = dr.GetInt32(dr.GetOrdinal("numemplacement"));
                empDispoPart.Dimension = dr.GetString(dr.GetOrdinal("dimension"));
                //empDispoPart.Idemplacementstockage = dr.GetInt32(dr.GetOrdinal("idemplacementstockage"));
                
                empDispoPart.Idville = dr.GetInt32(dr.GetOrdinal("idville"));
                empDispoPart.Idinfrastockage = dr.GetInt32(dr.GetOrdinal("idinfrastockage"));
                listemp.Add(empDispoPart);
            }
            cnx.Close();
            return listemp;
        }
    }

}
