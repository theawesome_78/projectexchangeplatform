﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fr.EQL.AI110.DLD_GE.Entities;
using MySql.Data.MySqlClient;


namespace Fr.EQL.AI110.DLD_GE.DataAccess
{
    public class InfrastockageDAO:DAO
    {
        public List<InfrastockageDetails> ToutInfraDispoParVille(int idville)
        {

            List<InfrastockageDetails> infrastockageDetails = new List<InfrastockageDetails>();
            // créer un objet connection :
            DbConnection cnx = new MySqlConnection();

            // 1 - Paramétrer la connection
            // :
            cnx.ConnectionString = CNX_STR;

            // 2 - Paramétrer la commande :
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT p.nomsociete, i.numrue, i.complement, i.libelevoie , 
                                p.numrue numruePart, p.complement complementPart, p.libelevoie libelevoiePart, 
                                v.codepostal, v.nom, i.idinfrastockage , e.numemplacement, e.idemplacementstockage, p.idpartenaire , COUNT(p.nomsociete) nbEmpDispo
                                FROM Don d
                                RIGHT OUTER JOIN emplacementstockage e ON e.idemplacementstockage = d.idemplacementstockage
                                RIGHT OUTER JOIN infrastockage i ON e.idinfrastockage = i.idinfrastockage
                                JOIN partenaire p ON p.idpartenaire = i.idpartenaire
                                JOIN ville v ON v.idville = i.idville
                                where (v.idville = @idville or @idville = 0)
                                AND d.idemplacementstockage IS NULL
                                GROUP BY idinfrastockage";

            cmd.Parameters.Add(new MySqlParameter("idville", idville));
            cnx.Open();

            DbDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                InfrastockageDetails infrastockageD = new InfrastockageDetails();
                infrastockageD.Nomsociete = dr.GetString(dr.GetOrdinal("nomsociete"));
                if (!dr.IsDBNull(dr.GetOrdinal("numrue")))
                {
                    infrastockageD.Numrue = dr.GetInt32(dr.GetOrdinal("numrue"));
                }
                if (!dr.IsDBNull(dr.GetOrdinal("complement")))
                {
                    infrastockageD.Complement = dr.GetChar(dr.GetOrdinal("complement"));
                }
                if (!dr.IsDBNull(dr.GetOrdinal("libelevoie")))
                {
                    infrastockageD.Libelevoie = dr.GetString(dr.GetOrdinal("libelevoie"));
                }
                infrastockageD.NumruePart = dr.GetInt32(dr.GetOrdinal("numruePart"));
                if (!dr.IsDBNull(dr.GetOrdinal("complementPart")))
                {
                    infrastockageD.ComplementPart = dr.GetChar(dr.GetOrdinal("complementPart"));
                }
                infrastockageD.LibelevoiePart = dr.GetString(dr.GetOrdinal("libelevoiePart"));
                infrastockageD.Codepostal = dr.GetInt32(dr.GetOrdinal("codepostal"));
                infrastockageD.Nom = dr.GetString(dr.GetOrdinal("nom"));
                infrastockageD.Idinfrastockage = dr.GetInt32(dr.GetOrdinal("idinfrastockage"));
                infrastockageD.Numemplacement = dr.GetInt32(dr.GetOrdinal("numemplacement"));
                infrastockageD.Idemplacementstockage = dr.GetInt32(dr.GetOrdinal("idemplacementstockage"));
                infrastockageD.NbEmpDispo = dr.GetInt32(dr.GetOrdinal("nbEmpDispo"));
                infrastockageDetails.Add(infrastockageD);
            }

            cnx.Close();

            return infrastockageDetails;
        }
        public List<InfrastockageDetails> ToutInfraDispo()
        {

            List<InfrastockageDetails> infrastockageDetails = new List<InfrastockageDetails>();
            // créer un objet connection :
            DbConnection cnx = new MySqlConnection();

            // 1 - Paramétrer la connection
            // :
            cnx.ConnectionString = CNX_STR;

            // 2 - Paramétrer la commande :
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT p.nomsociete, i.numrue, i.complement, i.libelevoie , 
                                p.numrue numruePart, p.complement complementPart, p.libelevoie libelevoiePart, 
                                v.codepostal, v.nom, i.idinfrastockage , e.numemplacement, e.idemplacementstockage, p.idpartenaire , COUNT(p.nomsociete) nbEmpDispo


                                FROM Don d
                                RIGHT OUTER JOIN emplacementstockage e ON e.idemplacementstockage = d.idemplacementstockage
                                RIGHT OUTER JOIN infrastockage i ON e.idinfrastockage = i.idinfrastockage
                                JOIN partenaire p ON p.idpartenaire = i.idpartenaire
                                JOIN ville v ON v.idville = i.idville

                                AND d.idemplacementstockage IS NULL

                                GROUP BY idinfrastockage
                                ";
         
            cnx.Open();

            DbDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                InfrastockageDetails infrastockageD = new InfrastockageDetails();
                infrastockageD.Nomsociete = dr.GetString(dr.GetOrdinal("nomsociete"));
                if (!dr.IsDBNull(dr.GetOrdinal("numrue")))
                {
                    infrastockageD.Numrue = dr.GetInt32(dr.GetOrdinal("numrue"));
                }
                if (!dr.IsDBNull(dr.GetOrdinal("complement")))
                {
                    infrastockageD.Complement = dr.GetChar(dr.GetOrdinal("complement"));
                }
                if (!dr.IsDBNull(dr.GetOrdinal("libelevoie")))
                { 
                    infrastockageD.Libelevoie = dr.GetString(dr.GetOrdinal("libelevoie"));
                }
                infrastockageD.NumruePart = dr.GetInt32(dr.GetOrdinal("numruePart"));
                if (!dr.IsDBNull(dr.GetOrdinal("complementPart")))
                {
                    infrastockageD.ComplementPart = dr.GetChar(dr.GetOrdinal("complementPart"));
                }
                infrastockageD.LibelevoiePart = dr.GetString(dr.GetOrdinal("libelevoiePart"));
                infrastockageD.Codepostal = dr.GetInt32(dr.GetOrdinal("codepostal"));
                infrastockageD.Nom = dr.GetString(dr.GetOrdinal("nom"));
                infrastockageD.Idinfrastockage = dr.GetInt32(dr.GetOrdinal("idinfrastockage"));
                infrastockageD.Numemplacement = dr.GetInt32(dr.GetOrdinal("numemplacement"));
                infrastockageD.Idemplacementstockage = dr.GetInt32(dr.GetOrdinal("idemplacementstockage"));
                infrastockageD.NbEmpDispo = dr.GetInt32(dr.GetOrdinal("nbEmpDispo"));
                infrastockageD.Idpartenaire = dr.GetInt32(dr.GetOrdinal("idpartenaire"));

                infrastockageDetails.Add(infrastockageD);
            }

            cnx.Close();

            return infrastockageDetails;
        }



    }
}
