﻿using Fr.EQL.AI110.DLD_GE.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GE.DataAccess
{
    public class CategorieDAO : DAO
    {
        public List<Categorie> GetAll()
        {
            List<Categorie> list = new List<Categorie>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "select * from categorie;";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Categorie categorie = new Categorie();
                    categorie.IdCategorie = dr.GetInt32(dr.GetOrdinal("idcategorie"));
                    categorie.Nom = dr.GetString(dr.GetOrdinal("nom"));
                    list.Add(categorie);
                }
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récuparation de toutes les catégories : " + ex.Message);
            }
            return list;
        }
        public Categorie GetById(int id)
        {
            Categorie cat = new Categorie();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "select * from categorie where idcategorie = @idcategorie;";
            cmd.Parameters.Add(new MySqlParameter("idcategorie", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    cat.IdCategorie = dr.GetInt32(dr.GetOrdinal("idcategorie"));
                    cat.Nom = dr.GetString(dr.GetOrdinal("nom"));
                }
                cnx.Close();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récuparation de la categorie idcategorie = " + id + " : " + ex.Message);
            }
            return cat;
        }
    }
}
